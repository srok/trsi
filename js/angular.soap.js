angular.module('angularSoap', [])

.factory("$soap",['$q','$rootScope',function($q,$rootScope){
	return {
		post: function(url, action, params){
			//console.log(url);
			var deferred = $q.defer();
			
			//Create SOAPClientParameters
			var soapParams = new SOAPClientParameters();

			////console.log(soapParams.printSchemaList());

			for(var param in params){
				soapParams.add(param, params[param]);
			}
			
			//Create Callback
			var soapCallback = function(e){
							//console.log(e);
				if(e == null || e=='net_error'){
					//console.log('reject');
					$rootScope.offline=true;
					deferred.reject("An error has occurred.");
				} else {
					$rootScope.offline=false;
					//console.log('resolve');
					deferred.resolve(e);
				}

			}
			
			SOAPClient.invoke(url, action, soapParams, true, soapCallback);

			return deferred.promise;
		},
		setCredentials: function(username, password){
			SOAPClient.username = username;
			SOAPClient.password = password;
		}
	}
}]);
